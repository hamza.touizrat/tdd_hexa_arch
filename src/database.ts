import sqlite3 from "sqlite3";

const createTables = async (db: sqlite3.Database) => {
  //Création de la table pizzas
  await new Promise<void>((resolve, reject) => {
    db.run(
      `CREATE TABLE IF NOT EXISTS pizzas (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name text, 
            slices int
            )`,
      (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(); // Table just created, creating some rows
        }
      }
    );
  });
  //Création de la table orders
  await new Promise<void>((resolve, reject) => {
    db.run(
      `CREATE TABLE IF NOT EXISTS orders (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            pizzaID int,
            nb_pers int
            )`,
      (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      }
    );
  });
  await new Promise<void>((resolve, reject) => {
    db.run(
      `CREATE TABLE IF NOT EXISTS ingredients (
            pizzaID int,
            ingredient_name string,
            qty int
            )`,
      (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      }
    );
  });
};
export const createDB = async (source: string) => {
  const dbCreated = await new Promise<sqlite3.Database>((resolve, reject) => {
    let db = new sqlite3.Database(source, (err) => {
      if (err) {
        console.error(err.message);
        reject(err);
        return;
      }
      console.log("Connected to the SQLite database.");
      resolve(db);
    });
  });
  await createTables(dbCreated);
  return dbCreated;
};
