import { Orders } from "../../business/domain/entities/Orders";
import { IPizzaCalculator } from "../../business/domain/interfaces/IpizzaCalculator";

export class PizzaCalculatorMock implements IPizzaCalculator {
  pizzaCount: [number, number][] = [
    // margherita
    [1, 1],
    // sicilienne
    [2, 2],
    // reine
    [3, 3],
  ];

  getPizzaCount(order: Orders): number {
    return Math.ceil(
      (this.pizzaCount.find((pizza) => pizza[0] === order.pizzaID)![1] *
        order.nb_pers) /
        6
    );
  }
}
