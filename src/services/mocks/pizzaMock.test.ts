//test PizzaMock

import { PizzaMock } from "./pizzaMock";

describe("PizzaMock", () => {
  it("should create a pizza", async () => {
    const pizzaMock = new PizzaMock();
    const pizzaToCreate = {
      name: "Pizza 1",
      slices: 2,
      id: 101000,
    };
    const pizzaCreated = await pizzaMock.createPizza(pizzaToCreate);
    expect(pizzaCreated).toEqual(pizzaToCreate);
  });
});
