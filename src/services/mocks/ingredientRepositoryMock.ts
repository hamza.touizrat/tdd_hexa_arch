import { Ingredient } from "../../business/domain/entities/Ingredient";
import { IingredientRepository } from "../../business/domain/interfaces/IingredientRepository";

type MockingredientRepositoryBdd = {
  [j in number]: Ingredient[];
};
export class MockingredientRepository implements IingredientRepository {
  mockingredientRepositoryBdd: MockingredientRepositoryBdd = {
    // reine
    3: [
      { tomate: 1 },
      { jambon: 1 },
      { oignons: 1 },
      { champignons: 1 },
      { olives: 1 },
      { origan: 1 },
    ],
    // sicilienne
    2: [
      { tomate: 1 },
      { jambon: 1 },
      { oignons: 1 },
      { champignons: 1 },
      { olives: 1 },
      { anchois: 1 },
      { origan: 1 },
    ],
    // margherita
    1: [{ tomate: 1 }, { mozzarella: 1 }, { origan: 1 }],
  };

  public async getIngredientsByPizza(TypePizza: number): Promise<Ingredient[]> {
    return this.mockingredientRepositoryBdd[TypePizza];
  }
}
