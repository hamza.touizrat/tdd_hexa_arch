import { OrderMock } from "./orderMock";

describe("OrderMock", () => {
  it("should return a list of orders", async () => {
    const orderMock = new OrderMock();
    const orders = await orderMock.getOrders();
    expect(orders).toHaveLength(2);
  });
  it("should create an order", async () => {
    const orderMock = new OrderMock();
    const orderToCreate = {
      pizzaID: 1,
      nb_pers: 2,
      id:1,
    };
    const orderCreated = await orderMock.createOrder(orderToCreate);
    expect(orderCreated).toEqual(orderToCreate);
  });
});
