import { Pizza } from "../../business/domain/entities/Pizza";
import { PizzaInterface } from "../../business/domain/interfaces/pizzas";

export class PizzaMock implements PizzaInterface {
  getPizzas(): Promise<Pizza[] | Error> {
    return new Promise((resolve, reject) => {
      const mockPizzas = [
        {
          id: 1,
          name: "Pizza 1",
          slices: 8,
        },
        {
          id: 2,
          name: "Pizza 2",
          slices: 6,
        },
        {
          id: 3,
          name: "Pizza 3",
          slices: 5,
        },
      ];
      resolve(mockPizzas as Pizza[]);
    });
  }

  createPizza(pizza: Pizza): Promise<Pizza | Error> {
    return new Promise((resolve, reject) => {
      resolve(pizza);
    });
  }
}
