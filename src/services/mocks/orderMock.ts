import { Orders } from "../../business/domain/entities/Orders";
import { OrderInterface } from "../../business/domain/interfaces/orders";

export class OrderMock implements OrderInterface {
  getOrders(): Promise<Orders[] | Error> {
    return new Promise((resolve, reject) => {
      const mockOrders = [
        {
          id: 1,
          pizzaID: 1,
          nb_pers: 2,
        },
        {
          id: 2,
          pizzaID: 2,
          nb_pers: 4,
        },
      ];
      resolve(mockOrders as Orders[]);
    });
  }

  createOrder(order: Orders): Promise<Orders | Error> {
    return new Promise((resolve, reject) => {
      resolve(order);
    });
  }
}
