import { sqlite3, Database } from "sqlite3";
import { Orders } from "../../business/domain/entities/Orders";
import { OrderInterface } from "../../business/domain/interfaces/orders";

export class OrderSQL implements OrderInterface {
  constructor(db: Database) {
    this.db = db;
  }
  db: Database;
  getOrders(): Promise<Error | Orders[]> {
    var sql =
      "SELECT * FROM orders INNER JOIN pizzas ON orders.pizzaID = pizzas.id";
    var params: any[] = [];
    return new Promise((resolve, reject) => {
      this.db.all(sql, params, (err: any, rows: any) => {
        if (err) {
          reject(new Error(err.message));
          return;
        }
        const orders: Orders[] = rows.map((e: any) => {
          return {
            id: e.id,
            pizzaID: e.pizzaID,
            pizza_name: e.name,
            nb_pers: e.nb_pers,
            nb_parts: e.slices * e.nb_pers,
          };
        });
        resolve(orders);
      });
    });
  }
  createOrder(orders: Orders): Promise<Error | Orders> {
    return new Promise((resolve, reject) => {
      var sql_pizza = "SELECT * FROM pizzas WHERE id = ?";
      var params_pizza = [orders.pizzaID];
      this.db.get(sql_pizza, params_pizza, (err_pizza: any, row_pizza: any) => {
        if (err_pizza) {
          reject(new Error(err_pizza.message));
          return;
        }
        if (!row_pizza) {
          reject(new Error("Pizza not found"));
          return;
        }

      var sql = "INSERT INTO orders (pizzaID, nb_pers) VALUES (?,?)";
      var params = [orders.pizzaID, orders.nb_pers];
      this.db.run(
        sql,
        params,
        function (err: { message: string | undefined }, result: any) {
          if (err) {
            return new Error(err.message);
          }
          resolve({
            id: this.lastID,
            pizzaID: orders.pizzaID,
            nb_pers: orders.nb_pers,
            nb_parts: row_pizza.slices * orders.nb_pers,
          });
        }
      );
    });
    } 
  )}
}
