import { Database } from "sqlite3";
import { IngredientCalculator } from "../../business/useCases/ingredientCalculator";
import { IingredientRepository } from "../../business/domain/interfaces/IingredientRepository";
import { Ingredient } from "../../business/domain/entities/Ingredient";

export class IngredientSQL implements IingredientRepository {
  private db: Database;

  constructor(db: Database) {
    this.db = db;
  }

  public async getIngredientsByPizza(TypePizza: number): Promise<Ingredient[]> {
    return new Promise((resolve, reject) => {
      this.db.all(
        `SELECT * FROM ingredient WHERE pizzaID = ?`,
        [TypePizza],
        (err, rows) => {
          if (err) {
            reject(err);
          }

          let ingredients: Ingredient[] = [];
          rows.forEach((value: any) => {
            let ing: Ingredient;
            ing.name = value.ingredient_name;
            ing.quantity = value.quantity;
            ingredients.push(ing);
          });

          resolve(ingredients);
        }
      );
    });
  }
}
