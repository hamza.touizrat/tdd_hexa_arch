import { createDB } from "../../database";
import { OrderSQL } from "./orderSQL";
import { PizzaSQL } from "./pizzaSQL";
import fs from "fs";

const path = "/tmp/db_test.sqlite";

describe("test Pizza SQL", () => {
  let orderSQL;
  let pizzaSQL;

  beforeAll(async () => {
    const db_test = await createDB(path); // Create the database
    orderSQL = new OrderSQL(db_test);
    pizzaSQL = new PizzaSQL(db_test);
  });

  afterAll(async () => {
    fs.unlinkSync(path); // Delete the database
  });

  it("should return pizzaID : 1 and nb_pers : 2 when create an order (pizzaID: 1, nb_pers: 2) in database and the pizza with ID : 1 is (name: Pizza 3, slices: 2) ", async () => {
    const pizzaToCreate = {
      name: "Pizza 3",
      slices: 2,
    };
    await pizzaSQL.createPizza(pizzaToCreate); //First pizza in database so pizzaID = 1
    const orderToCreate = {
      pizzaID: 1,
      nb_pers: 2,
    };
    const orderCreated = await orderSQL.createOrder(orderToCreate);
    expect(orderCreated.pizzaID).toBe(1);
    expect(orderCreated.nb_pers).toBe(2);
  });

  it("should reject with an error when creating an order with a pizzaID that does not exist in the database", async () => {
    const orderToCreate = {
      pizzaID: 999, // A non-existing pizzaID
      nb_pers: 2,
    };

    await expect(orderSQL.createOrder(orderToCreate)).rejects.toThrow(
      "Pizza not found"
    );
  });

  it("should return the length of the list of all orders when an order have the same pizzaID as the unique pizza in database", async () => {
    const pizzaToCreate = {
      name: "Pizza 3",
      slices: 2,
    };
    await pizzaSQL.createPizza(pizzaToCreate); //First pizza in database so pizzaID = 1
    const orderList = await orderSQL.getOrders();
    expect(orderList.length).toBe(1);
  });

  it("should return 4 for number of parts of the order (pizzaID: 1, nb_pers: 2) when the pizza with ID : 1 is (name: Pizza 3, slices: 2) ", async () => {
    const pizzaToCreate = {
      name: "Pizza 3",
      slices: 2,
    };
    await pizzaSQL.createPizza(pizzaToCreate); //First pizza in database so pizzaID = 1
    const orderList = await orderSQL.getOrders();
    expect(orderList[0].nb_parts).toBe(4);
  });
});
