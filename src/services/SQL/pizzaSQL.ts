import { sqlite3, Database } from "sqlite3";
import { Pizza } from "../../business/domain/entities/Pizza";
import { PizzaInterface } from "../../business/domain/interfaces/pizzas";

export class PizzaSQL implements PizzaInterface {
  constructor(db: Database) {
    this.db = db;
  }
  db: Database;
  getPizzas(): Promise<Pizza[] | Error> {
    var sql = "select * from pizzas";
    var params: any[] = [];
    return new Promise((resolve, reject) => {
      this.db.all(sql, params, (err: any, rows: any) => {
        if (err) {
          reject(new Error(err.message));
          return;
        }
        const pizzas: Pizza[] = rows.map((e: any) => ({
          id: e.id,
          name: e.name,
          slices: e.slices,
        }));
        resolve(pizzas);
      });
    });
  }

  createPizza(pizza: Pizza): Promise<Pizza | Error> {
    return new Promise((resolve, reject) => {
      var sql = "INSERT INTO pizzas (name, slices) VALUES (?,?)";
      var params = [pizza.name, pizza.slices];
      this.db.run(
        sql,
        params,
        function (err: { message: string | undefined }, result: any) {
          if (err) {
            return new Error(err.message);
          }
          resolve({
            id: this.lastID,
            name: pizza.name,
            slices: pizza.slices,
          });
        }
      );
    });
  }
}
