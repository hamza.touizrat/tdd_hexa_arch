import { createDB } from "../../database";
import { PizzaSQL } from "./pizzaSQL";
import fs from "fs";

const path = "/tmp/db_test.sqlite";

describe("test Pizza SQL", () => {
  let pizzaSQL;

  beforeAll(async () => {
    const db_test = await createDB(path); // Create the database
    pizzaSQL = new PizzaSQL(db_test);
  });

  afterAll(async () => {
    fs.unlinkSync(path); // Delete the database
  });

  it("should store a pizza in database", async () => {
    const pizzaToCreate = {
      name: "Pizza 3",
      slices: 2,
    };
    const pizzaCreated = await pizzaSQL.createPizza(pizzaToCreate);
    expect(pizzaCreated.name).toBe("Pizza 3");
  });

  it("should return the list of all pizzas", async () => {
    const pizzaList = await pizzaSQL.getPizzas();
    expect(pizzaList[0].name).toBe("Pizza 3");
  });
});
