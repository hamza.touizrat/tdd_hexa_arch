import { createDB } from "./database";
import { getPizzaController, postPizzaController } from "./controllers/pizzas";
import { getOrderController, postOrderController } from "./controllers/orders";
import { PizzaSQL } from "./services/SQL/pizzaSQL";
import { OrderSQL } from "./services/SQL/orderSQL";
import express from "express";
import { Database } from "sqlite3";
import { IngredientCalculator } from "./business/useCases/ingredientCalculator";
import { PizzaCalculatorMock } from "./services/mocks/pizzaCalculatorMock";
import { getIngredientsController } from "./controllers/ingredients";
import { IngredientSQL } from "./services/SQL/ingredientSQL";

export const routes = (db: Database) => {
  const app = express();

  app.use(express.json());

  const pizzaSQL = new PizzaSQL(db);

  app.get("/pizzas", getPizzaController(pizzaSQL));

  app.post("/pizzas", postPizzaController(pizzaSQL));

  const orderSQL = new OrderSQL(db);

  app.get("/orders", getOrderController(orderSQL));

  app.post("/orders", postOrderController(orderSQL, pizzaSQL));

  const ingredientSQL = new IngredientSQL(db);

  app.get(
    "/ingredients",
    getIngredientsController(ingredientSQL, new PizzaCalculatorMock(), orderSQL)
  );

  return app;
};
