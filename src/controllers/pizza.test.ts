import { setOriginalNode } from "typescript";
import { PizzaMock } from "../services/mocks/pizzaMock";
import { getPizzaController, postPizzaController } from "./pizzas";

const pizzaMock = new PizzaMock();

describe("createPizza", () => {
  it("should throw a 400 error if name is empty", async () => {
    const mReq = { body: { name: "", slices: 3 } };
    const mRes = {
      status: jest.fn(),
      send: jest.fn(),
      json: jest.fn(),
    };
    mRes.status = jest.fn().mockReturnValue(mRes);
    const mNext = jest.fn();
    pizzaMock.createPizza = jest.fn();
    const postPizzaMockControllers = postPizzaController(pizzaMock);
    await postPizzaMockControllers(mReq, mRes, mNext);
    expect(mRes.status).toBeCalledWith(400);
  });
  it("should throw a 400 error if slices is empty", async () => {
    const mReq = { body: { name: "Pepe", slices: 0 } };
    const mRes = {
      status: jest.fn(),
      send: jest.fn(),
      json: jest.fn(),
    };
    mRes.status = jest.fn().mockReturnValue(mRes);
    const mNext = jest.fn();
    pizzaMock.createPizza = jest.fn();
    const postPizzaMockControllers = postPizzaController(pizzaMock);
    await postPizzaMockControllers(mReq, mRes, mNext);
    expect(mRes.status).toBeCalledWith(400);
  });
  it("should create a pizza and return a 201 code of success", async () => {
    const mReq = { body: { name: "Regina", slices: 3 } };
    const mRes = {
      status: jest.fn(),
      send: jest.fn(),
      json: jest.fn(),
    };
    mRes.status = jest.fn().mockReturnValue(mRes);
    const mNext = jest.fn();
    pizzaMock.createPizza = jest.fn();
    const postPizzaMockControllers = postPizzaController(pizzaMock);
    await postPizzaMockControllers(mReq, mRes, mNext);
    expect(mRes.status).toBeCalledWith(201);
  });
  it("listPizzas should return a 201 code of success", async () => {
    const mReq = { body: {} };
    const mRes = {
      status: jest.fn(),
      send: jest.fn(),
      json: jest.fn(),
    };
    mRes.status = jest.fn().mockReturnValue(mRes);
    const mNext = jest.fn();
    pizzaMock.createPizza = jest.fn();
    await getPizzaController(pizzaMock)(mReq, mRes, mNext);
    expect(mRes.status).toBeCalledWith(200);
  });
});
