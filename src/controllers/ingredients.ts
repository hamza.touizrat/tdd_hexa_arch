import { createDB } from "../database";
import { createOrder } from "../business/useCases/createOrder";
import { listOrders } from "../business/useCases/listOrders";
import { OrderInterface } from "../business/domain/interfaces/orders";
import { OrderSQL } from "../services/SQL/orderSQL";
import { IngredientCalculator } from "../business/useCases/ingredientCalculator";
import { PizzaCalculatorMock } from "../services/mocks/pizzaCalculatorMock";
import { MockingredientRepository } from "../services/mocks/ingredientRepositoryMock";
import { IPizzaCalculator } from "../business/domain/interfaces/IpizzaCalculator";
import { IingredientRepository } from "../business/domain/interfaces/IingredientRepository";

export const getIngredientsController = (
  ingredientRepository: IingredientRepository,
  pizzaCalculator: IPizzaCalculator,
  orderInterface: OrderInterface
) => {
  return async (_req: any, res: any, _next: any) => {
    const ingredientCalculator = new IngredientCalculator();
    ingredientCalculator.ingredientRepository = ingredientRepository;
    ingredientCalculator.pizzaCalculator = new PizzaCalculatorMock();

    let idOrder = _req.query.idOrder;

    const listOrder = await listOrders(orderInterface);

    if (listOrder instanceof Error) {
      return res.status(400).send("Error");
    }

    let order = listOrder.find((order) => order.id == idOrder);

    res.status(200).send(ingredientCalculator.getIngredient(order));
  };
};
