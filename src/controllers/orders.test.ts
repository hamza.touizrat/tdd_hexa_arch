import { setOriginalNode } from "typescript";
import { OrderMock } from "../services/mocks/orderMock";
import { getOrderController, postOrderController } from "./orders";
import { PizzaMock } from "../services/mocks/pizzaMock";

const orderMock = new OrderMock();
const pizzaMock = new PizzaMock();

describe("createOrder", () => {
  it("should create an order and return a 201 code of success when body contains an order with pizzaID : 1 and nb_pers : 2", async () => {
    const mReq = { body: { pizzaID: 1, nb_pers: 2 } };
    const mRes = {
      status: jest.fn(),
      send: jest.fn(),
      json: jest.fn(),
    };
    mRes.status = jest.fn().mockReturnValue(mRes);
    const mNext = jest.fn();
    orderMock.createOrder = jest.fn();
    const postOrderMockControllers = postOrderController(orderMock, pizzaMock);
    await postOrderMockControllers(mReq, mRes, mNext);
    expect(mRes.status).toBeCalledWith(201);
  });
  it("listOrders should return a 201 code of success when body is empty", async () => {
    const mReq = { body: {} };
    const mRes = {
      status: jest.fn(),
      send: jest.fn(),
      json: jest.fn(),
    };
    mRes.status = jest.fn().mockReturnValue(mRes);
    const mNext = jest.fn();
    orderMock.createOrder = jest.fn();
    await getOrderController(orderMock)(mReq, mRes, mNext);
    expect(mRes.status).toBeCalledWith(200);
  });
  it("should throw a 400 error if pizzaID is 0 when body is (pizzaID : 0, nb_pers : 2)", async () => {
    const mReq = { body: { pizzaID: 0, nb_pers: 2 } };
    const mRes = {
      status: jest.fn(),
      send: jest.fn(),
      json: jest.fn(),
    };
    mRes.status = jest.fn().mockReturnValue(mRes);
    const mNext = jest.fn();
    orderMock.createOrder = jest.fn();
    const postOrderMockControllers = postOrderController(orderMock, pizzaMock);
    await postOrderMockControllers(mReq, mRes, mNext);
    expect(mRes.status).toBeCalledWith(400);
  });
  it("should throw a 400 error if nb_pers is 0 when body is (pizzaID : 1, nb_pers : 0)", async () => {
    const mReq = { body: { pizzaID: 1, nb_pers: 0 } };
    const mRes = {
      status: jest.fn(),
      send: jest.fn(),
      json: jest.fn(),
    };
    mRes.status = jest.fn().mockReturnValue(mRes);
    const mNext = jest.fn();
    orderMock.createOrder = jest.fn();
    const postOrderMockControllers = postOrderController(orderMock, pizzaMock);
    await postOrderMockControllers(mReq, mRes, mNext);
    expect(mRes.status).toBeCalledWith(400);
  });
  it("should throw a 400 error if pizzaID not in pizzaList", async () => {
    const mReq = { body: { pizzaID: 10000, nb_pers: 3 } };
    const mRes = {
      status: jest.fn(),
      send: jest.fn(),
      json: jest.fn(),
    };
    mRes.status = jest.fn().mockReturnValue(mRes);
    const mNext = jest.fn();
    orderMock.createOrder = jest.fn();
    const postOrderMockControllers = postOrderController(orderMock, pizzaMock);
    await postOrderMockControllers(mReq, mRes, mNext);
    expect(mRes.status).toBeCalledWith(400);
  });
});
