import { createDB } from "../database";
import { createOrder } from "../business/useCases/createOrder";
import { listOrders } from "../business/useCases/listOrders";
import { OrderInterface } from "../business/domain/interfaces/orders";
import { OrderSQL } from "../services/SQL/orderSQL";
import { listPizzas } from "../business/useCases/listPizzas";
import { PizzaInterface } from "../business/domain/interfaces/pizzas";

/**
 *
 * Récupération de la liste des commandes dans le service choisi
 */
export const getOrderController = (orderInterface: OrderInterface) => {
  return async (_req: any, res: any, _next: any) => {
    const listOrder = await listOrders(orderInterface);
    res.status(200).send(listOrder);
  };
};

/**
 *
 * Création d'une commande dans le service choisi
 */
export const postOrderController = (
  orderInterface: OrderInterface,
  pizzaInterface: PizzaInterface
) => {
  return async (req: { body: any }, res: any, _next: any) => {
    const order = req.body;

    if (!req.body.pizzaID) {
      return res.status(400).send("Pizza ID is required");
    }
    if (!req.body.nb_pers) {
      return res.status(400).send("Number of persons is required");
    }
    const pizzaList = await listPizzas(pizzaInterface);
    if (Array.isArray(pizzaList)) {
      const pizzaIDList = pizzaList.map((pizza) => pizza.id);
      if (!pizzaIDList.includes(req.body.pizzaID)) {
        return res.status(400).send("You can only order pizzas from the menu");
      }
    }
    const orderCreated = await createOrder(order, orderInterface);
    res.status(201).send(orderCreated);
  };
};
