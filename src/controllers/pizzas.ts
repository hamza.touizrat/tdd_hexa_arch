import { createDB } from "../database";
import { createPizza } from "../business/useCases/createPizza";
import { listPizzas } from "../business/useCases/listPizzas";
import { PizzaInterface } from "../business/domain/interfaces/pizzas";
import { PizzaSQL } from "../services/SQL/pizzaSQL";

/**
 * Récupération de la liste des pizzas dans le service choisi
 */
export const getPizzaController = (pizzaInterface: PizzaInterface) => {
  return async (_req: any, res: any, _next: any) => {
    const listPizza = await listPizzas(pizzaInterface);
    res.status(200).send(listPizza);
  };
};

/**
 * Création d'une pizza dans le service choisi
 *
 */
export const postPizzaController = (pizzaInterface: PizzaInterface) => {
  return async (req: { body: any }, res: any, _next: any) => {
    const pizza = req.body;

    if (!req.body.name) {
      return res.status(400).send("Name is required");
    }
    if (!req.body.slices) {
      return res.status(400).send("Slices is required");
    }
    const pizzaCreated = await createPizza(pizza, pizzaInterface);
    res.status(201).send(pizzaCreated);
  };
};
