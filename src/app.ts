import { routes } from "./routes";
import { createDB } from "./database";

const run = async () => {
  // Initiating the database
  const db = await createDB("db.sqlite");
  const app = routes(db);

  app.listen(3000, () => {
    console.log("Listening on port 3000");
  });
};

run();
