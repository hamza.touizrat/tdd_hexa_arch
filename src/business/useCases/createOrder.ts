import { Orders } from "../domain/entities/Orders";
import { OrderInterface } from "../domain/interfaces/orders";

/**
 *
 * Un order est créé par le client et est ensuite disponible pour le pizzaiolo avec le nombre de parts nécessaires pour la commande
 */
export const createOrder = (
  newOrder: Orders,
  orderInterface: OrderInterface
) => {
  if (newOrder.pizzaID === 0) {
    return new Error("ID pizza is required");
  }
  if (newOrder.nb_pers === 0) {
    return new Error("Number of persons is required");
  }

  return orderInterface.createOrder(newOrder);
};
