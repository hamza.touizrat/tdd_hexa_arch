import { PizzaMock } from "../../services/mocks/pizzaMock";
import { createPizza } from "./createPizza";

const pizzaMock = new PizzaMock();

describe("test a pizzaiolo can create a pizza", () => {
  it("should create a pizza with the correct name", async () => {
    const pizzaToCreate = {
      name: "Pizza 1",
      slices: 2,
      id: 101000,
    };
    pizzaMock.createPizza = jest.fn().mockResolvedValue(pizzaToCreate);
    const pizzaCreated = await createPizza(pizzaToCreate, pizzaMock);
    expect(pizzaCreated.name).toBe("Pizza 1");
  });
  it("should not create a pizza when name is not given", async () => {
    const pizzaToCreate = {
      name: "",
      slices: 2,
      id: 101000,
    };
    pizzaMock.createPizza = jest.fn().mockResolvedValue(pizzaToCreate);
    const error = await createPizza(pizzaToCreate, pizzaMock);
    expect("message" in error).toBe(true);
  });
  it("should not create a pizza when slice is not given", async () => {
    const pizzaToCreate = {
      name: "Pizza 1",
      slices: 0,
      id: 101000,
    };
    pizzaMock.createPizza = jest.fn().mockResolvedValue(pizzaToCreate);
    const error = await createPizza(pizzaToCreate, pizzaMock);
    expect("message" in error).toBe(true);
  });
});
