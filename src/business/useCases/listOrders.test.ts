import { OrderMock } from "../../services/mocks/orderMock";
import { listOrders } from "./listOrders";

const orderMock = new OrderMock();

describe("listOrder", () => {
  it("should return 2 orders when Mock is call", async () => {
    const orders = await listOrders(orderMock);
    expect(orders).toHaveLength(2);
    expect(orders[0]).toStrictEqual({"id": 1, "nb_pers": 2, "pizzaID": 1});
    expect(orders[1]).toStrictEqual({"id": 2, "nb_pers": 4, "pizzaID": 2});
  });
});
