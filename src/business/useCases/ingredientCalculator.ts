import { IPizzaCalculator } from "../domain/interfaces/IpizzaCalculator";
import { Ingredient } from "../domain/entities/Ingredient";
import { IingredientRepository } from "../domain/interfaces/IingredientRepository";
import { Orders } from "../domain/entities/Orders";

export class IngredientCalculator {
  ingredientRepository: IingredientRepository;
  pizzaCalculator: IPizzaCalculator;

  public async getIngredient(Command: Orders): Promise<Ingredient[]> {
    let ingredients: Ingredient[];
    const nb_pizza: number = this.pizzaCalculator.getPizzaCount(Command);

    await this.ingredientRepository
      .getIngredientsByPizza(Command.pizzaID)
      .then((data) => {
        ingredients = data;
      })
      .catch((err) => {
        console.log(err);
      });

    // for any ingrendient return the right number of ingredient for the number of pizza
    ingredients = ingredients.map((ingredient) => {
      const key = Object.keys(ingredient)[0];
      ingredient[key] = ingredient[key] * nb_pizza;
      return ingredient;
    });

    return ingredients;
  }
}
