import { expect } from "chai";
import * as sinon from "sinon";
import { IngredientCalculator } from "./ingredientCalculator";
import { PizzaCalculatorMock } from "../../services/mocks/pizzaCalculatorMock";
import { MockingredientRepository } from "../../services/mocks/ingredientRepositoryMock";
import { Orders } from "../domain/entities/Orders";
describe("IngredientCalculator", () => {
  it("Test if the right number of ingredients are returned for 1 Pizza Margarita", async () => {
    //Arrange
    const order: Orders = {
      nb_pers: 1,
      pizzaID: 1,
      id: 1,
    };
    const ingredientCalculator = new IngredientCalculator();
    ingredientCalculator.ingredientRepository = new MockingredientRepository();
    ingredientCalculator.pizzaCalculator = new PizzaCalculatorMock();

    //Act
    const ingredients = await ingredientCalculator.getIngredient(order);

    //Assert
    console.log(ingredients);
    expect(ingredients).to.deep.equal([
      {
        tomate: 1,
      },
      {
        mozzarella: 1,
      },
      {
        origan: 1,
      },
    ]); // Need to check the right number of ingredients
  });

  it("Test if the right number of ingredients are returned for 3 Pizza sicilienne", async () => {
    //Arrange
    const order: Orders = {
      nb_pers: 8,
      pizzaID: 2,
      id: 1,
    };
    const ingredientCalculator = new IngredientCalculator();
    ingredientCalculator.ingredientRepository = new MockingredientRepository();
    ingredientCalculator.pizzaCalculator = new PizzaCalculatorMock();

    //Act
    const ingredients = await ingredientCalculator.getIngredient(order);

    //Assert
    expect(ingredients).to.deep.equal([
      {
        tomate: 3,
      },
      {
        jambon: 3,
      },
      {
        oignons: 3,
      },
      {
        champignons: 3,
      },
      {
        olives: 3,
      },
      {
        anchois: 3,
      },
      {
        origan: 3,
      },
    ]); // Need to check the right number of ingredients
  });
});
