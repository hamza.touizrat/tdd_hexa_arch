import { OrderMock } from "../../services/mocks/orderMock";
import { createOrder } from "./createOrder";

const orderMock = new OrderMock();

describe("createOrder", () => {
  it("should create an order with pizzaID : 1, nb_pers : 3 and id : 101000 when create an order with pizzaID, nb_pers ans id", async () => {
    const orderToCreate = {
      pizzaID: 1,
      nb_pers: 3,
      id: 101000,
    };
    orderMock.createOrder = jest.fn().mockResolvedValue(orderToCreate);
    const orderCreated = await createOrder(orderToCreate, orderMock);
    expect(orderCreated).toStrictEqual({"id": 101000, "nb_pers": 3, "pizzaID": 1});
  });

  it("should not create an order when pizzaID is 0", async () => {
    const orderToCreate = {
      pizzaID: 0,
      nb_pers: 3,
      id: 101000,
    };
    orderMock.createOrder = jest.fn().mockResolvedValue(orderToCreate);
    const error = await createOrder(orderToCreate, orderMock);
    expect("message" in error).toBe(true);
  });

  it("should not create an order when nb_pers is 0", async () => {
    const orderToCreate = {
      pizzaID: 1,
      nb_pers: 0,
      id: 101000,
    };
    orderMock.createOrder = jest.fn().mockResolvedValue(orderToCreate);
    const error = await createOrder(orderToCreate, orderMock);
    expect("message" in error).toBe(true);
  });
});
