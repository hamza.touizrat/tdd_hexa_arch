import { Pizza } from "../domain/entities/Pizza";
import { PizzaInterface } from "../domain/interfaces/pizzas";

/**
 *
 * Liste les pizzas disponibles à la commande (pour le client)
 */
export const listPizzas = (pizzaInterface: PizzaInterface) => {
  return pizzaInterface.getPizzas();
};
