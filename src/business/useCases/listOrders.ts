import { Orders } from "../domain/entities/Orders";
import { OrderInterface } from "../domain/interfaces/orders";

/**
 *
 * Liste les commades pour le pizzaiolo
 */
export const listOrders = (orderInterface: OrderInterface) => {
  return orderInterface.getOrders();
};
