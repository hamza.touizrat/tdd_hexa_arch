import { Pizza } from "../domain/entities/Pizza";
import { PizzaInterface } from "../domain/interfaces/pizzas";

/**
 *
 * Crée une pizza disponible à la commande (fonction pour le pizzaiolo)
 */
export const createPizza = (
  newPizza: Pizza,
  pizzaInterface: PizzaInterface
) => {
  if (newPizza.name === "") {
    return new Error("Name is required");
  }
  if (newPizza.slices === 0) {
    return new Error("Slices is required");
  }

  return pizzaInterface.createPizza(newPizza);
};
