export type Ingredient = {
    [key: string]: number;
};