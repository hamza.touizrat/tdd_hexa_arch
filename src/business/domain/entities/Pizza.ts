export type Pizza = {
  name: string;
  slices: number;
  id?: number;
};
