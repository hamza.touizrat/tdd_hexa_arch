import { Ingredient } from "../entities/Ingredient";

export  interface IingredientRepository {
    getIngredientsByPizza(TypePizza: number):  Promise<Ingredient[]>;
}

