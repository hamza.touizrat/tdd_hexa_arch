import { Orders } from "../entities/Orders";

export interface OrderInterface {
    getOrders(): Promise<Orders[] | Error>;
    createOrder(orders: Orders): Promise<Orders | Error>;
}

