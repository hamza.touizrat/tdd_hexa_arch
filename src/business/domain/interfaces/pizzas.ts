import { Pizza } from "../entities/Pizza";

export interface PizzaInterface {
    getPizzas(): Promise<Pizza[]|Error>;
    createPizza(pizza: Pizza): Promise<Pizza|Error>;
}
