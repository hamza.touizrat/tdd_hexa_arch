import { Orders } from "../entities/Orders";

export interface IPizzaCalculator {
    getPizzaCount(order: Orders): number;
}