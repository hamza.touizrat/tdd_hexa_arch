# Projet d'architecture hexagonale développée en TDD

## Introduction

Cette application implémente la logique métier d'un pizzaiolo. Ce dernier propose à ses clients une liste de pizzas. Il peut créer des nouvelles pizzas pour les ajouter au menu. Chaque pizza a un nom et un nombre de part que le client doit commander pour être rassasié.

Le client, quant à lui, peut passer une commande et donc créer une nouvelle commande. Pour cela, il donne la pizza qu'il souhaite ainsi que le nombre de personnes qui va consommer. Le pizzaiolo récupère la liste des commandes à traiter. Dans cette liste, le nombre de parts de pizzas qu'il doit produire par commande est indiqué, pour faciliter son travail.

## Structure du code

### Business

#### Domain

La partie domaine fait le lien entre le métier et les services permettant de répondre aux besoins du métier.
Il est constitué des entités qui définissent les objets métier et des interfaces qui permettent d'avoir, ici, deux types de services de gestion de données selon le besoin (SQL et Mock pour les tests).

#### Cas d'utilisation métier

La partie Use Case fait appel aux objets du domaine.
Cette partie est compréhensible par le métier et renvoie les objets nécessaires à son fonctionnement :

- la liste des commandes avec le nombre de parts à produire par commande
- création d'une nouvelle pizza pour l'ajouter à la carte
- affichage de la carte pour les clients
- création d'une nouvelle commande à ajouter à la liste des commandes à produire
- calcul puis affichage de la liste des ingrédients nécessaire pour satisfaire une commande

Cette partie fait appel aux fonctions SQL permettant de traiter la requête.

### Controllers

Les controlleurs gèrent les requêtes HTTP POST et GET déclenchées par l'utilisateur. Ils déclenchent les cas d'utilisation métier en formattant correctement les entrées et sorties pour faire la conversion HTTP (queryParam, body, ...) et métier.

### Services

#### Service SQL

Ce service est celui de prod, il permet de d'ajouter dans une base de données SQL ou d'afficher des éléments des table pizzas et orders de cette bdd. Il permet également de récupérer les ingrédients nécessaire au fonctionnement de la logique métier.

#### Service Mock

Ce service permet de faire des tests sur les controllers et sur les use cases.

## Set up

### Installation

```bash
yarn
```

### Run

Pour lancer le serveur :

```bash
yarn start
```

Pour lancer les tests

```bash
yarn test
```
